package com.example.hribar.listaexample.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.hribar.listaexample.R;
import com.example.hribar.listaexample.fragment.adapter.ViewPagerAdapter;
import com.example.hribar.listaexample.utils.ZoomOutPageTransformer;

import java.util.ArrayList;

public class GalleryFragment extends Fragment {

    private int[] galleryImages;
    private int position;
    private ViewPagerAdapter mPagerAdapter;
    private ViewPager mPager;

    public static GalleryFragment newInstance( int[] galleryImages, int position) {

        Bundle args = new Bundle();
        args.putIntArray("galleryImages", galleryImages);
        args.putInt("position", position);
        GalleryFragment fragment = new GalleryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        galleryImages = getArguments().getIntArray("galleryImages");
        position = getArguments().getInt("position");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_gallery, container, false);

        mPager = (ViewPager) rootView.findViewById(R.id.pager);
        mPager.setPageTransformer(true, new ZoomOutPageTransformer());
        mPagerAdapter = new ViewPagerAdapter(getChildFragmentManager(),galleryImages);
        mPager.setAdapter(mPagerAdapter);
        mPager.setCurrentItem(position);

        return rootView;
    }
}
