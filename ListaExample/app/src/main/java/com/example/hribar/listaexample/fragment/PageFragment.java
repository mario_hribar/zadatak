package com.example.hribar.listaexample.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.hribar.listaexample.R;
import com.example.hribar.listaexample.utils.TouchImageView;


public class PageFragment extends Fragment{

    private TouchImageView touchImageView;
    private int image;

    public static PageFragment newInstance(int image) {

        Bundle args = new Bundle();
        args.putInt("image", image);
        PageFragment fragment = new PageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        image = getArguments().getInt("image");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_page,container,false);

        touchImageView = (TouchImageView) rootView.findViewById(R.id.image_page);
        touchImageView.setImageResource(image);

        return rootView;
    }

}
