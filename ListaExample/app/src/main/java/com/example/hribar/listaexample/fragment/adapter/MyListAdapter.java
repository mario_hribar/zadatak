package com.example.hribar.listaexample.fragment.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.hribar.listaexample.R;
import com.example.hribar.listaexample.fragment.MyListFragment;
import com.example.hribar.listaexample.objects.Hotels;

import java.util.ArrayList;

public class MyListAdapter extends RecyclerView.Adapter<MyListAdapter.ViewHolder> {

    private ArrayList<Hotels> mHotelsData;
    public Context context;
    public ClickListener clickListener;
    private int[]images;

    public MyListAdapter(ArrayList<Hotels> mHotelsData, Context context) {
        this.mHotelsData = mHotelsData;
        this.context = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        final Hotels hotels = mHotelsData.get(position);
        holder.name.setText(hotels.getName());
        holder.address.setText(hotels.getAddress());
        holder.post_number.setText(hotels.getPost_number());

        images = hotels.getImage();
        Glide.with(context).load(images[0]).centerCrop().into(holder.image);

    }

    @Override
    public int getItemCount() {
        return mHotelsData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView name,address,post_number;
        public ImageView image;

        public ViewHolder(View v) {
            super(v);

            name = (TextView) v.findViewById(R.id.tvName);
            address = (TextView) v.findViewById(R.id.tvAddress);
            post_number = (TextView) v.findViewById(R.id.tvPostNumber);
            image = (ImageView) v.findViewById(R.id.image);

            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null){
                clickListener.ItemClick(v, getAdapterPosition());
            }
        }
    }
    public interface ClickListener {
        void ItemClick(View view, int position);
    }

    public void setClickListener(ClickListener clickListener){

        this.clickListener=clickListener;
    }


}
