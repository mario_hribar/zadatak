package com.example.hribar.listaexample.fragment;


import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.example.hribar.listaexample.R;
import com.example.hribar.listaexample.fragment.adapter.MyListAdapter;
import com.example.hribar.listaexample.objects.Hotels;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;

public class MyListFragment extends Fragment implements MyListAdapter.ClickListener {

    private ArrayList<Hotels> hotelsList;
    private int[] image1 = {R.drawable.vila_maria,R.drawable.villa_maria_2,R.drawable.villa_maria_3,R.drawable.villa_maria_4},
                  image2 = {R.drawable.hotel_dukat,R.drawable.hotel_dukat_2,R.drawable.hotel_dukat_3,R.drawable.hotel_dukat_4},
            image3 = {R.drawable.apartman_klara,R.drawable.apartman_klara_2,R.drawable.apartman_klara_3,R.drawable.apartman_klara_4,R.drawable.apartman_klara_5},
            image4 = {R.drawable.hotel_grabovac,R.drawable.hotel_grabovac_2,R.drawable.hotel_grabovac_3,R.drawable.hotel_grabovac_4},
            image5 = {R.drawable.hotel_mozart,R.drawable.hotel_mozart_2,R.drawable.hotel_mozart_3,R.drawable.hotel_mozart_4},
            image6 = {R.drawable.hotel_plitvice,R.drawable.hotel_plitvice_2,R.drawable.hotel_plitvice_3,R.drawable.hotel_plitvice_4},
            image7 = {R.drawable.hotel_suzy,R.drawable.hotel_suzy_2,R.drawable.hotel_suzy_3,R.drawable.hotel_suzy_4},
            image8 = {R.drawable.kuca_dama,R.drawable.kuca_dama_2,R.drawable.kuca_dama_3,R.drawable.kuca_dama_4},
            image9 = {R.drawable.vila_duzevic,R.drawable.vila_duzevic_2,R.drawable.vila_duzevic_3,R.drawable.vila_duzevic_4,R.drawable.vila_duzevic_5},
            image10 = {R.drawable.zlatni_lug,R.drawable.zlatni_lug_2,R.drawable.zlatni_lug_3,R.drawable.zlatni_lug_4};


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_my_list, container, false);

        hotelsList = new ArrayList<Hotels>();

        hotelsList.add(new Hotels("Pansion Villa-Maria", "Dr.Franje Tuđmana 14", "34000 Požega", image1));
        hotelsList.add(new Hotels("Villa Duževic", "A.G.Matoša 4", "20250 Orebić", image9));
        hotelsList.add(new Hotels("Hotel Dukat", "Trg Sv.Križa 4", "33515 Orahovica", image2));
        hotelsList.add(new Hotels("Hotel Suzy", "Šetalište Petra Preradovića 9", "21000 Split", image7));
        hotelsList.add(new Hotels("Hotel Plitvice", "Plitvička jezera", "HR 53231 Plitvička jezera Hrvatska - Europa", image6));
        hotelsList.add(new Hotels("Kuća Dama", "Manđerova 14", "21000 Split", image8));
        hotelsList.add(new Hotels("Hotel Grabovac", "Plitvička jezera", "HR 53231 Plitvička jezera Hrvatska - Europa", image4));
        hotelsList.add(new Hotels("Hotel Mozart", "M.Tita 138", "51410 Opatija", image5));
        hotelsList.add(new Hotels("Zlatni Lug", "Donji Emovci 28a", "34000 Požega", image10));
        hotelsList.add(new Hotels("Apartman Klara", "Put Znjana 20 B", "21000 Split", image3));

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.my_recycler_view);
        recyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity()).build());

        MyListAdapter myListAdapter = new MyListAdapter(hotelsList, getActivity());
        myListAdapter.setClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(myListAdapter);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.first_screen));
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);

    }

    @Override
    public void ItemClick(View view, int position) {

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment_container, DetailsFragment.newInstance(hotelsList.get(position)));
        ft.addToBackStack(null);
        ft.commit();
    }

}
