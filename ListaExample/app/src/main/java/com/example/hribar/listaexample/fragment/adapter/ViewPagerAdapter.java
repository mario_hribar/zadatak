package com.example.hribar.listaexample.fragment.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.hribar.listaexample.fragment.GalleryFragment;
import com.example.hribar.listaexample.fragment.PageFragment;


public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private int[] galleryImages;

    public ViewPagerAdapter(FragmentManager fm, int[] galleryImages) {
        super(fm);
        this.galleryImages = galleryImages;

    }

    @Override
    public Fragment getItem(int position) {
        return PageFragment.newInstance(galleryImages[position]);
    }

    @Override
    public int getCount() {
        return galleryImages.length;
    }

}
