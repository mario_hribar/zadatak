package com.example.hribar.listaexample.objects;

import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Hotels implements Parcelable {

    private String name,address,post_number;
    private int[] image;


    public Hotels() {}

    public Hotels(String name, String address, String post_number, int[] image) {
        this.name = name;
        this.address = address;
        this.post_number = post_number;
        this.image = image;
    }


    protected Hotels(Parcel in) {
        name = in.readString();
        address = in.readString();
        post_number = in.readString();
        image = in.createIntArray();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(post_number);
        dest.writeIntArray(image);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Hotels> CREATOR = new Creator<Hotels>() {
        @Override
        public Hotels createFromParcel(Parcel in) {
            return new Hotels(in);
        }

        @Override
        public Hotels[] newArray(int size) {
            return new Hotels[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPost_number() {
        return post_number;
    }

    public void setPost_number(String post_number) {
        this.post_number = post_number;
    }

    public int[] getImage() {
        return image;
    }

    public void setImage(int[] image) {
        this.image = image;
    }
}
