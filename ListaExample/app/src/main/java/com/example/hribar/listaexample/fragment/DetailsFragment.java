package com.example.hribar.listaexample.fragment;

import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.hribar.listaexample.R;
import com.example.hribar.listaexample.fragment.adapter.ViewPagerAdapter;
import com.example.hribar.listaexample.objects.Hotels;

import java.util.ArrayList;

public class DetailsFragment extends Fragment implements View.OnClickListener{

    private Hotels hotels;
    TextView name, address,postNumber,description;
    ImageView img1,img2,img3,imgMain;
    RatingBar ratingBar;

    private int[] images;

    public static DetailsFragment newInstance(Hotels hotels) {

        Bundle args = new Bundle();

        DetailsFragment fragment = new DetailsFragment();
        args.putParcelable("hotels", hotels);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hotels = getArguments().getParcelable("hotels");


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_details, container, false);

        imgMain = (ImageView) rootView.findViewById(R.id.imageView);
        img1 = (ImageView) rootView.findViewById(R.id.image1);
        img1.setOnClickListener(this);
        img2 = (ImageView) rootView.findViewById(R.id.image2);
        img2.setOnClickListener(this);
        img3 = (ImageView) rootView.findViewById(R.id.image3);
        img3.setOnClickListener(this);

        ratingBar = (RatingBar) rootView.findViewById(R.id.ratingBar2);

        name = (TextView) rootView.findViewById(R.id.textView);
        address = (TextView) rootView.findViewById(R.id.textView2);
        postNumber = (TextView) rootView.findViewById(R.id.textView3);
        description = (TextView) rootView.findViewById(R.id.textView4);

        images = hotels.getImage();
        Glide.with(getActivity()).load(images[0]).centerCrop().into(imgMain);
        Glide.with(getActivity()).load(images[1]).centerCrop().into(img1);
        Glide.with(getActivity()).load(images[2]).centerCrop().into(img2);
        Glide.with(getActivity()).load(images[3]).centerCrop().into(img3);


        name.setText(hotels.getName());
        address.setText(hotels.getAddress());
        postNumber.setText(hotels.getPost_number());
        description.setText(getString(R.string.text_description));

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(hotels.getName());
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.image1:
                gallery(1);
                break;

            case R.id.image2:
                gallery(2);
                break;

            case R.id.image3:
                gallery(3);
                break;
        }
    }

    public void gallery(int position){

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment_container, GalleryFragment.newInstance(images,position));
        ft.addToBackStack(null);
        ft.commit();
    }

}
